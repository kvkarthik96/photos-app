import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/Utils/PrintUtil.dart';

class SharedPreferenceUtil {
  PrintUtil printUtil = PrintUtil();

  addSharedPref(String prefKey, String prefValue) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    printUtil.printMsg('$prefKey, $prefValue');
    prefs.setString(prefKey, prefValue);
  }

  addIntSharedPref(String prefKey, int prefValue) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    printUtil.printMsg('$prefKey, $prefValue');
    prefs.setInt(prefKey, prefValue);
  }

  Future<String> getSharedPref(String prefKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String prefValue = prefs.getString(prefKey);
    return prefValue;
  }

  addListSharedPref(String prefKey, List prefValue) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    printUtil.printMsg('$prefKey, $prefValue');
    prefs.setStringList(prefKey, prefValue);
  }

  Future<List> getListSharedPref(String prefKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List prefValue = prefs.getStringList(prefKey);
    return prefValue;
  }
}
