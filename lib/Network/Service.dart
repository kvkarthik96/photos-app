import 'dart:async';
import 'dart:core';
import 'package:test_app/Network/Network.dart';

class Service {
  Network _network = new Network();

  static final getUserdetailsUrl =
      "https://jsonplaceholder.typicode.com/users/";
  static final getAlbumUrl =
      'https://jsonplaceholder.typicode.com/albums?userId=';
  static final getPhotosUrl =
      'https://jsonplaceholder.typicode.com/photos?albumId=';

  Future<Object> getUserdetails({String userId}) {
    return _network.get(getUserdetailsUrl + userId).then((dynamic res) {
      return res;
    });
  }

  Future<List<dynamic>> getAlbumDetails({String userId}) {
    return _network.get(getAlbumUrl + userId).then((dynamic res) {
      return res;
    });
  }

  Future<List<dynamic>> getPhotosDetails({String albumId}) {
    return _network.get(getPhotosUrl + albumId).then((dynamic res) {
      return res;
    });
  }
}
