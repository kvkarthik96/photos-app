import 'package:get_it/get_it.dart';
import 'package:test_app/Routers/NavigatorService.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
}
