import 'package:flutter/material.dart';
import 'package:test_app/Resources/RouteConst.dart';
import 'package:test_app/Screens/AlbumList/AlbumListPage.dart';
import 'package:test_app/Screens/LaunchScreen.dart';
import 'package:test_app/Screens/Login/LoginPage.dart';
import 'package:test_app/Screens/PhotosList/PhotosListPage.dart';
import 'package:test_app/Screens/Tabbar/Tabbar.dart';
import 'package:test_app/Screens/ViewPage/ViewScreen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case RouteConst.routeDefault:
        return MaterialPageRoute(builder: (_) => LaunchScreen());
        break;
      case RouteConst.routeLoginPage:
        return MaterialPageRoute(builder: (_) => LoginPage());
        break;
      case RouteConst.routeListPage:
        if (args != "" && args != null) {
          Map<String, dynamic> data;
          data = args;
          return MaterialPageRoute(
              builder: (_) => AlbumListPage(
                    id: data['id'],
                    name: data['name'],
                  ));
        } else {
          return MaterialPageRoute(builder: (_) => AlbumListPage());
        }

        break;
      case RouteConst.routePhotosPage:
        if (args != "" && args != null) {
          Map<String, dynamic> data;
          data = args;
          return MaterialPageRoute(
              builder: (_) => PhotosListPage(
                    albumId: data['albumId'],
                    albumName: data['albumName'],
                  ));
        } else {
          return MaterialPageRoute(builder: (_) => PhotosListPage());
        }
        break;
      case RouteConst.routeViewPage:
        if (args != "" && args != null) {
          Map<String, dynamic> data;
          data = args;
          return MaterialPageRoute(
              builder: (_) => ViewPage(
                    imgUrl: data['imgUrl'],
                    name: data['name'],
                  ));
        } else {}

        break;

      case RouteConst.routeTabbarPage:
        return MaterialPageRoute(builder: (_) => TabbarPage());

        break;
      default:
        return _errorRoute(settings.name);
    }
  }

  static Route<dynamic> _errorRoute(pageName) {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.error_outline),
              Text(
                "Error Routing Page Not Found : " + pageName.toString(),
                style: TextStyle(fontSize: 18.0),
              )
            ],
          ),
        ),
      );
    });
  }
}
