class RouteConst {
  static const String routeDefault = "/";
  static const routeLoginPage = "/LoginPage";
  static const routeListPage = "/ListPage";
  static const routePhotosPage = "/PhotosPag";
  static const routeViewPage = "/ViewPage";
  static const routeTabbarPage = "/TabbarPage";
}
