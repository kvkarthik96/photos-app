import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ViewPage extends StatefulWidget {
  final String imgUrl;
  final String name;

  const ViewPage({Key key, this.imgUrl, this.name = ""}) : super(key: key);
  @override
  _ViewPageState createState() => _ViewPageState();
}

class _ViewPageState extends State<ViewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(widget.name),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
          ),
          color: Colors.white,
          splashColor: Colors.purple,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: CachedNetworkImage(
          fit: BoxFit.fitWidth,
          height: MediaQuery.of(context).size.width,
          width: MediaQuery.of(context).size.width,
          imageUrl: widget.imgUrl,
          placeholder: (context, url) =>
              Center(child: CircularProgressIndicator()),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      ),
    );
  }
}
