import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:test_app/Resources/RouteConst.dart';
import 'package:test_app/Screens/Login/Widgets/GetTextFieldStyle.dart';
import 'package:test_app/Screens/Login/Widgets/HandleBackButton.dart';
import 'package:test_app/Screens/Login/Widgets/ValidationClass.dart';
import 'package:test_app/Screens/Login/bloc/login_bloc.dart';
import 'package:test_app/Utils/HelperUtil.dart';
import 'package:test_app/Utils/LoadingUtil.dart';
import 'package:test_app/Utils/SharedPreferenceUtil.dart';
import 'package:test_app/Utils/ToastUtil.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  ValueNotifier<bool> _saving = ValueNotifier<bool>(false);
  final loginBloc = LoginBloc();
  final passwordKey = GlobalKey<FormFieldState>();
  final nameKey = GlobalKey<FormFieldState>();
  final TextEditingController nameController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  final FocusNode nameFocusNode = FocusNode();
  final FocusNode passwordFocusNode = FocusNode();

  SharedPreferenceUtil _sharedPreference = SharedPreferenceUtil();
  //  GlobalKey<FormState> _key = new GlobalKey();

  @override
  void initState() {
    super.initState();
    _sharedPreference.addSharedPref('isNewUser', '0'); // new user
  }

  @override
  void dispose() {
    loginBloc.close();
    nameFocusNode.dispose();
    passwordFocusNode.dispose();
    _saving.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
        cubit: loginBloc,
        buildWhen: (prevState, state) {
          if (state is LoginInitial) {
            return false;
          } else if (state is GetUserDetailsState) {
            if (state.photosDetails != null) {
              _saving.value = false;
              Map<String, dynamic> data = {
                'id': state.photosDetails['id'].toString(),
                'name': state.photosDetails['name']
              };

              _sharedPreference.addSharedPref(
                  'userId', state.photosDetails['id'].toString());
              _sharedPreference.addSharedPref(
                  'userName', state.photosDetails['name']);
              Navigator.of(context)
                  .pushNamed(RouteConst.routeListPage, arguments: data);
            } else {
              ToastUtil().showMsg("something went wrong!!!", Colors.black,
                  Colors.white, 12.0, "short", "bottom");
            }

            return false;
          }
          return false;
        },
        builder: (context, state) {
          if (state is LoginLoadedState) {
            return WillPopScope(
              onWillPop: () {
                return handleBack(context);
              },
              child: ValueListenableBuilder(
                builder: (BuildContext context, bool saving, Widget child) {
                  return ModalProgressHUD(
                    progressIndicator: LoadingUtil.ballRotate(context),
                    dismissible: false,
                    inAsyncCall: saving,
                    child: child,
                  );
                },
                valueListenable: _saving,
                child: Scaffold(
                  backgroundColor: Colors.white,
                  body: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 15.0, right: 15.0, top: 20.0, bottom: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Welcome to Rejolut.com',
                              style: TextStyle(
                                  color: Colors.orange,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18.0),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Container(
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 0.0),
                                  child: Text(
                                    "User Name",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                        fontStyle: FontStyle.normal),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Container(
                              child: TextFormField(
                                key: nameKey,
                                onTap: () {},
                                focusNode: nameFocusNode,
                                inputFormatters: [
                                  FilteringTextInputFormatter.deny(
                                      RegExp('[ ]')),
                                ],
                                controller: nameController,
                                validator: validateName,
                                keyboardType: TextInputType.emailAddress,
                                maxLengthEnforced: true,
                                style: getStyle(),
                                decoration: getInputDecoration('User Name', 1),
                                onChanged: (text) {
                                  nameKey.currentState.validate();
                                },
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 0.0),
                                  child: Text(
                                    "Password",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                        fontStyle: FontStyle.normal),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Container(
                              child: TextFormField(
                                key: passwordKey,
                                focusNode: passwordFocusNode,
                                onTap: () {},
                                maxLength: 18,
                                inputFormatters: [
                                  FilteringTextInputFormatter.deny(
                                      RegExp('[ ]')),
                                ],
                                controller: passwordController,
                                validator: validatePassword,
                                keyboardType: TextInputType.text,
                                maxLengthEnforced: true,
                                style: getStyle(),
                                decoration: getInputDecoration('Password', 2),
                                onChanged: (password) {
                                  passwordKey.currentState.validate();
                                },
                              ),
                            ),
                            SizedBox(height: 20.0),
                            RaisedButton(
                              onPressed: () {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                if (!nameKey.currentState.validate()) {
                                  nameFocusNode.requestFocus();
                                } else if (!passwordKey.currentState
                                    .validate()) {
                                  passwordFocusNode.requestFocus();
                                } else {
                                  var userName = nameController.text.split("@");
                                  HelperUtil.checkInternetConnection()
                                      .then((internet) {
                                    if (internet) {
                                      _saving.value = true;
                                      loginBloc.add(GetUserDetailsEvent(
                                          userName[0].length.toString()));
                                    } else {
                                      ToastUtil().showMsg(
                                          "no internet connection",
                                          Colors.black,
                                          Colors.white,
                                          12.0,
                                          "short",
                                          "bottom");
                                    }
                                  });
                                }
                              },
                              child: Text('Sign In'),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
        });
  }
}
