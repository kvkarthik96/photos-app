part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class GetUserDetailsEvent extends LoginEvent {
  final String userId;
  GetUserDetailsEvent(this.userId);
}
