part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginLoadingState extends LoginState {}

class LoginLoadedState extends LoginState {}

class LoginErrorState extends LoginState {}

class GetUserDetailsState extends LoginState {
  final Map<String, dynamic> photosDetails;

  GetUserDetailsState(this.photosDetails);
}
