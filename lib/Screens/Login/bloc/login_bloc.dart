import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:test_app/Network/Service.dart';
import 'package:test_app/Utils/HelperUtil.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginLoadedState());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is GetUserDetailsEvent) {
      yield* _mapGetUserDetailsEventDatatoState(event);
    }
  }

  Stream<LoginState> _mapGetUserDetailsEventDatatoState(
      GetUserDetailsEvent event) async* {
    Map<String, dynamic> photosDetails;

    getUserDetails() async {
      await HelperUtil.checkInternetConnection().then((internet) async {
        if (internet) {
          await Service().getUserdetails(userId: event.userId).then((respObj) {
            photosDetails = respObj;
          });
        }
      });
    }

    yield LoginInitial();
    await getUserDetails();
    yield GetUserDetailsState(photosDetails);
  }
}
