import 'package:flutter/material.dart';

// Title Style
TextStyle getStyle() {
  return TextStyle(
      fontSize: 14.0, fontWeight: FontWeight.normal, color: Colors.black);
}

// TextField decoration
InputDecoration getInputDecoration(hintText, int from) {
  return InputDecoration(
    filled: true,
    fillColor: Colors.white,
    hintText: hintText,
    counterText: "",
    contentPadding: EdgeInsets.fromLTRB(08.0, 0.0, 5.0, 0.0),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(4)),
      borderSide:
          BorderSide(color: Colors.blue, width: 1.0, style: BorderStyle.solid),
    ),
    border: const OutlineInputBorder(),
    hintStyle: TextStyle(fontSize: 14, color: Colors.black38),
    errorMaxLines: 3,
    errorStyle:
        TextStyle(color: Colors.red, fontSize: 14, fontStyle: FontStyle.normal),
  );
}
