// Username Validation
String validateName(String value) {
  value = value.replaceAll(RegExp(' +'), '');

  String pattern = r'(^[a-zA-Z]*$)';
  RegExp regex = new RegExp(pattern);

  var userName = value.split("@rejolut.com");

  if (value.length == 0) {
    return "Please enter user name";
  } else if (!value.endsWith("@rejolut.com") || !regex.hasMatch(userName[0])) {
    return "Please enter user name (Example admin@rejolut.com)";
  } else {
    return null;
  }
}

// Password validation
String validatePassword(String value) {
  String pattern =
      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  RegExp regex = new RegExp(pattern);
  value = value.replaceAll(RegExp(' +'), '');
  if (value.length == 0) {
    return "Please enter your password";
  }
  if (!regex.hasMatch(value) || value.length < 8) {
    return " Password should be at least 8 letters, one Capital letter, one small letter, one number and one special character";
  } else {
    return null;
  }
}
