import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  List<Map<String, dynamic>> items = [
    {
      "name": "Wheat/Rice Flour",
      "price": "RS 50",
      "quantity": 1,
      "image": "assets/images/flour.png",
    },
    {
      "name": "Olive oil",
      "price": "RS 50",
      "quantity": 1,
      "image": "assets/images/oil.png",
    },
    {
      "name": "Potato Chips",
      "price": "RS 200",
      "quantity": 1,
      "image": "assets/images/chips.png",
    },
  ];

  var selItem = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
              child: Container(
                width: 250,
                height: 50,
                child: Form(
                  child: Container(
                    decoration: new BoxDecoration(
                      color: const Color(0xffffffff),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.search,
                      maxLength: 50,
                      maxLengthEnforced: true,
                      onSaved: (String val) {},
                      onChanged: (text) {
                        // onTextChange();
                      },
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: 15.0,
                      ),
                      inputFormatters: [
                        FilteringTextInputFormatter.deny(RegExp(r"\s\s")),
                        FilteringTextInputFormatter.deny(RegExp(
                            r'(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])')),
                      ],
                      decoration: InputDecoration(
                          hintText: "Search Items",
                          focusColor: Colors.green,
                          counterText: "",
                          enabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.black, width: 1.0),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(9.0),
                          ),
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.w500, color: Colors.grey),
                          suffixIcon: IconButton(
                            icon: Icon(Icons.search),
                            color: Colors.black,
                            onPressed: () {},
                          )),
                    ),
                  ),
                ),
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                height: 60,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selItem = 1;
                        });
                      },
                      child: Text(
                        "Flores & Pules Oil",
                        style: TextStyle(
                            color: selItem == 1 ? Colors.blue : Colors.black,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(
                      " | ",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontWeight: FontWeight.w600),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selItem = 2;
                        });
                      },
                      child: Text(
                        "Ghee Snacks",
                        style: TextStyle(
                            color: selItem == 2 ? Colors.blue : Colors.black,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(
                      " | ",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontWeight: FontWeight.w600),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selItem = 3;
                        });
                      },
                      child: Text(
                        "Dry Fruites",
                        style: TextStyle(
                            color: selItem == 3 ? Colors.blue : Colors.black,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(
                      " | ",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontWeight: FontWeight.w600),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selItem = 4;
                        });
                      },
                      child: Text(
                        "View All",
                        style: TextStyle(
                            color: selItem == 4 ? Colors.blue : Colors.black,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      padding: const EdgeInsets.all(10.0),
                      child: ListTile(
                        leading: Container(
                          width: 75.0,
                          height: 75.0,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(items[index]['image']),
                                fit: BoxFit.contain),
                          ),
                        ),
                        title: Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Container(
                                    child: Text(
                                      items[index]['price'],
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Container(
                                    child: Text(
                                      items[index]['name'],
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      items[index]["quantity"] =
                                          items[index]["quantity"] + 1;
                                    });
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 1,
                                              color: Colors.black
                                                  .withOpacity(0.7)),
                                          borderRadius:
                                              BorderRadius.circular(2.0)),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 7.0,
                                            right: 7.0,
                                            top: 4.0,
                                            bottom: 4.0),
                                        child: Text(
                                          "${items[index]["quantity"]} kg",
                                          style: TextStyle(
                                              fontSize: 9.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        trailing: Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                          child: Container(
                            child: Text(
                              "Add to cart",
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
