import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/Resources/RouteConst.dart';

class LaunchScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<LaunchScreen> {
  var payloadData;

  @override
  void initState() {
    super.initState();

    sessionHandling(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  sessionHandling(context) async {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // String isAlreadyLogin =
    //     prefs.getString("isNewUser") ?? "0"; //0.new user 1.old user
    // String userId = prefs.getString("userId") ?? "";
    // String userName = prefs.getString("userName") ?? "";

    // if (isAlreadyLogin == "1") {
    //   Map<String, dynamic> data = {'id': userId, 'name': userName};
    //   Navigator.of(context)
    //       .pushNamed(RouteConst.routeListPage, arguments: data);
    // } else {
    //   Navigator.of(context).pushNamed(
    //     RouteConst.routeLoginPage,
    //   );
    // }

    Navigator.of(context).pushNamed(
      RouteConst.routeTabbarPage,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
