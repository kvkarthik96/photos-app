import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/Resources/RouteConst.dart';
import 'package:test_app/Screens/AlbumList/Widgets/HandleBackButton.dart';
import 'package:test_app/Utils/SharedPreferenceUtil.dart';

Widget appBar(BuildContext context, String name) {
  return AppBar(
    title: Text(name),
    leading: IconButton(
      icon: Icon(
        Icons.arrow_back_ios,
      ),
      color: Colors.white,
      splashColor: Colors.purple,
      onPressed: () {
        handleBack(context);
      },
    ),
    actions: [
      Center(
        child: Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: InkWell(
            onTap: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.clear();
              SharedPreferenceUtil _sharedPreference = SharedPreferenceUtil();
              _sharedPreference.addSharedPref('isNewUser', '0');

              Navigator.of(context).pushReplacementNamed(
                RouteConst.routeLoginPage,
              );
            },
            child: Text(
              'Logout',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    ],
  );
}
