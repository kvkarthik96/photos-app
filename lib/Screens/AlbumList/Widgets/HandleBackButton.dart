import 'dart:io';

import 'package:flutter/material.dart';

handleBack(BuildContext context) {
  return showDialog<bool>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text("Do you want to exit?"),
        actions: <Widget>[
          FlatButton(
            child: new Text('Yes', style: TextStyle(color: Colors.black)),
            onPressed: () {
              exit(0);
            },
          ),
          FlatButton(
            child: new Text('No', style: TextStyle(color: Colors.black)),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
        ],
      );
    },
  );
}
