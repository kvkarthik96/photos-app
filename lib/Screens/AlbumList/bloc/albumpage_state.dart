part of 'albumpage_bloc.dart';

@immutable
abstract class AlbumpageState {}

class AlbumpageInitial extends AlbumpageState {}

class AlbumpageLoadingState extends AlbumpageState {}

class AlbumpageLoadedState extends AlbumpageState {
  final List<dynamic> albumDetails;

  AlbumpageLoadedState(this.albumDetails);
}

class AlbumpageErrorState extends AlbumpageState {}
