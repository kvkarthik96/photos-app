part of 'albumpage_bloc.dart';

@immutable
abstract class AlbumpageEvent {}

class GetAlbumDetailsEvent extends AlbumpageEvent {
  final String userId;

  GetAlbumDetailsEvent(this.userId);
}
