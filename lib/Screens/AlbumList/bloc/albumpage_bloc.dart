import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:test_app/Network/Service.dart';
import 'package:test_app/Utils/HelperUtil.dart';

part 'albumpage_event.dart';
part 'albumpage_state.dart';

class AlbumpageBloc extends Bloc<AlbumpageEvent, AlbumpageState> {
  AlbumpageBloc() : super(AlbumpageLoadingState());

  @override
  Stream<AlbumpageState> mapEventToState(
    AlbumpageEvent event,
  ) async* {
    if (event is GetAlbumDetailsEvent) {
      yield* _mapGetAlbumDetailsEventDatatoState(event);
    }
  }

  Stream<AlbumpageState> _mapGetAlbumDetailsEventDatatoState(
      GetAlbumDetailsEvent event) async* {
    List<dynamic> albumDetails;

    try {
      getAlbumDetails() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await Service()
                .getAlbumDetails(userId: event.userId)
                .then((respObj) {
              albumDetails = respObj;
            });
          }
        });
      }

      yield AlbumpageLoadingState();
      await getAlbumDetails();
      yield AlbumpageLoadedState(albumDetails);
    } catch (e) {
      yield AlbumpageErrorState();
    }
  }
}
