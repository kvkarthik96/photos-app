import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/Resources/RouteConst.dart';
import 'package:test_app/Screens/AlbumList/Widgets/Appbar.dart';
import 'package:test_app/Screens/AlbumList/Widgets/HandleBackButton.dart';
import 'package:test_app/Screens/AlbumList/Widgets/Shimmer.dart';
import 'package:test_app/Utils/ErrorWidget.dart';
import 'package:test_app/Utils/HelperUtil.dart';
import 'package:test_app/Utils/NoInternetUtil.dart';
import 'package:test_app/Utils/SharedPreferenceUtil.dart';
import 'package:test_app/Utils/ToastUtil.dart';

import 'bloc/albumpage_bloc.dart';

class AlbumListPage extends StatefulWidget {
  final String id;
  String name;

  AlbumListPage({Key key, this.id = "", this.name = ""}) : super(key: key);
  @override
  _AlbumListPageState createState() => _AlbumListPageState();
}

class _AlbumListPageState extends State<AlbumListPage> {
  final albumListBloc = AlbumpageBloc();
  bool isNetworkConnected = false;
  var _connectivitySubscription;
  SharedPreferenceUtil _sharedPreference = SharedPreferenceUtil();

  @override
  void initState() {
    super.initState();
    _sharedPreference.addSharedPref('isNewUser', '1'); // new user
    getDetails();
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        isNetworkConnected = true;
      } else {
        isNetworkConnected = false;
      }
    });

    onNetworkChange();
  }

  Future getDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('isNewUser') != null &&
        prefs.getString('isNewUser') == '1') {
      widget.name = prefs.getString('isName') ?? "";
      var userId = prefs.getString('userId') ?? "1";
      albumListBloc.add(GetAlbumDetailsEvent(userId));
    } else {
      albumListBloc.add(GetAlbumDetailsEvent(widget.id));
    }
  }

  void onNetworkChange() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
        } else {
          isNetworkConnected = true;
          albumListBloc.add(GetAlbumDetailsEvent(widget.id));
        }
      });
    });
  }

  void dispose() {
    albumListBloc.close();
    _connectivitySubscription.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AlbumpageBloc, AlbumpageState>(
        cubit: albumListBloc,
        builder: (context, state) {
          if (state is AlbumpageLoadingState) {
            return WillPopScope(
              onWillPop: () {
                return handleBack(context);
              },
              child: Scaffold(
                appBar: appBar(context, widget.name),
                body: !isNetworkConnected
                    ? NoInternetUtil(retryInternetCallBack: callApiService)
                    : albumListShimmer(),
              ),
            );
          } else if (state is AlbumpageLoadedState) {
            return WillPopScope(
              onWillPop: () {
                return handleBack(context);
              },
              child: Scaffold(
                appBar: appBar(context, widget.name),
                body: !isNetworkConnected
                    ? NoInternetUtil(retryInternetCallBack: callApiService)
                    : Container(
                        child: ListView.builder(
                            itemCount: state.albumDetails.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: Text(state.albumDetails[index]["title"]),
                                onTap: () {
                                  Map<String, dynamic> data = {
                                    "albumId": state.albumDetails[index]
                                            ['userId']
                                        .toString(),
                                    "albumName": state.albumDetails[index]
                                        ['title']
                                  };
                                  Navigator.of(context).pushNamed(
                                      RouteConst.routePhotosPage,
                                      arguments: data);
                                },
                              );
                            }),
                      ),
              ),
            );
          } else {
            return WillPopScope(
              onWillPop: () {
                return handleBack(context);
              },
              child: Scaffold(
                appBar: appBar(context, widget.name),
                body: ErrorWidgetClass(
                  retryInternetCallBack: callApiService,
                ),
              ),
            );
          }
        });
  }

  void callApiService() {
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        albumListBloc.add(GetAlbumDetailsEvent(widget.id));
      } else {
        ToastUtil().showMsg("no internet connection", Colors.black,
            Colors.white, 12.0, "short", "bottom");
      }
    });
  }
}
