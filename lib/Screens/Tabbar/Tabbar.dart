import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:test_app/Screens/Cart.dart';
import 'package:test_app/Screens/Home.dart';
import 'package:test_app/Screens/MyList.dart';
import 'package:test_app/Screens/Search.dart';

class TabbarPage extends StatefulWidget {
  TabbarPage({
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => TabbarPageState();
}

class TabbarPageState extends State<TabbarPage> {
  int _selectedIndex = 0;

  //Helper Class

  final PageStorageBucket bucket = PageStorageBucket();

  BuildContext myContext;
  String langId;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (_selectedIndex == 0) {
          return showDialog<bool>(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Text("Are you sure you want to exit?"),
                  actions: <Widget>[
                    FlatButton(
                      child: new Text("Yes",
                          style: TextStyle(color: Colors.black)),
                      onPressed: () {
                        exit(0);
                      },
                    ),
                    FlatButton(
                      child:
                          new Text("No", style: TextStyle(color: Colors.black)),
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                    ),
                  ],
                );
              });
        } else {
          // return Navigator.pushReplacement(
          //     context,
          //     MaterialPageRoute(
          //         builder: (BuildContext context) =>
          //             DashboardTabBar(configured: true)));
        }
      },
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: PreferredSize(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.lightGreen,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25.0),
                    bottomRight: Radius.circular(25.0),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.only(),
                        child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.menu,
                            color: Colors.white,
                            size: 35,
                          ),
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: Icon(
                              Icons.location_on,
                              color: Colors.black,
                              size: 25,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              "Madiwala, Bangalore - 00000",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              child: IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.person,
                                  size: 25,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              preferredSize: Size.fromHeight(100)),
          body: SafeArea(
            child: Stack(
              children: <Widget>[
                bodyContent,
                Container(
                  child: bottomNavigationBar,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget get bodyContent {
    if (_selectedIndex == 0) {
      return Home();
    }
    if (_selectedIndex == 1) {
      return Search();
    }
    if (_selectedIndex == 2) {
      return MyList();
    }
    if (_selectedIndex == 3) {
      return Cart();
    }
  }

  Widget get bottomNavigationBar {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          height: 70,
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black,
              )
            ],
          ),
          child: BottomNavigationBar(
            onTap: (int index) {
              setState(() {
                _selectedIndex = index;
              });
            },
            currentIndex: _selectedIndex,
            items: [
              BottomNavigationBarItem(
                icon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 25, bottom: 5.0),
                  child: Icon(
                    Icons.home,
                    size: 30,
                  ),
                ),
                activeIcon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 25, bottom: 5.0),
                  child: Icon(
                    Icons.home,
                    size: 30,
                    color: Colors.black,
                  ),
                ),
                title: Center(
                    child: Text(
                  "Home",
                  maxLines: 2,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                )),
              ),
              BottomNavigationBarItem(
                icon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 25, bottom: 5.0),
                  child: Icon(
                    Icons.search,
                    size: 30,
                    // color: Colors.black,
                  ),
                ),
                activeIcon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 25, bottom: 5.0),
                  child: Icon(
                    Icons.search,
                    size: 30,
                    color: Colors.black,
                  ),
                ),
                title: Center(
                    child: Text(
                  "Search",
                  maxLines: 2,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                )),
              ),
              BottomNavigationBarItem(
                icon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 25, bottom: 5.0),
                  child: Icon(
                    Icons.description,
                    size: 30,
                    // color: Colors.black,
                  ),
                ),
                activeIcon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 25, bottom: 5.0),
                  child: Icon(
                    Icons.description,
                    size: 30,
                    color: Colors.black,
                  ),
                ),
                title: Center(
                    child: Text(
                  "My List",
                  maxLines: 2,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                )),
              ),
              BottomNavigationBarItem(
                icon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 25, bottom: 5.0),
                  child: Icon(
                    Icons.child_friendly,
                    size: 30,
                    //  color: Colors.black,
                  ),
                ),
                activeIcon: Padding(
                  padding: EdgeInsets.only(left: 25, right: 25, bottom: 5.0),
                  child: Icon(
                    Icons.child_friendly,
                    size: 30,
                    color: Colors.black,
                  ),
                ),
                title: Center(
                    child: Text(
                  "Cart",
                  maxLines: 2,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                )),
              ),
            ],
            elevation: 8.0,
            unselectedItemColor: Colors.grey,
            backgroundColor: Colors.white,
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Colors.blue,
            showUnselectedLabels: true,
          ),
        ),
      ],
    );
  }
}
