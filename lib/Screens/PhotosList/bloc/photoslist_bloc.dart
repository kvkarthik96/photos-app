import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:test_app/Network/Service.dart';
import 'package:test_app/Utils/HelperUtil.dart';

part 'photoslist_event.dart';
part 'photoslist_state.dart';

class PhotoslistBloc extends Bloc<PhotoslistEvent, PhotoslistState> {
  PhotoslistBloc() : super(PhotoslistLoadingState());

  @override
  Stream<PhotoslistState> mapEventToState(
    PhotoslistEvent event,
  ) async* {
    if (event is GetPhotosetailsEvent) {
      yield* _mapGetAlbumDetailsEventDatatoState(event);
    }
  }

  Stream<PhotoslistState> _mapGetAlbumDetailsEventDatatoState(
      GetPhotosetailsEvent event) async* {
    List<dynamic> photosDetails;

    try {
      getPhotosDetails() async {
        await HelperUtil.checkInternetConnection().then((internet) async {
          if (internet) {
            await Service()
                .getPhotosDetails(albumId: event.albumId)
                .then((respObj) {
              photosDetails = respObj;
            });
          }
        });
      }

      yield PhotoslistLoadingState();
      await getPhotosDetails();
      yield PhotoslistLoadedState(photosDetails);
    } catch (e) {
      yield PhotoslistErrorState();
    }
  }
}
