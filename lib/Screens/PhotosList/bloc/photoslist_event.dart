part of 'photoslist_bloc.dart';

@immutable
abstract class PhotoslistEvent {}

class GetPhotosetailsEvent extends PhotoslistEvent {
  final String albumId;
  GetPhotosetailsEvent(this.albumId);
}
