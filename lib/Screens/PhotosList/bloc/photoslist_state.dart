part of 'photoslist_bloc.dart';

@immutable
abstract class PhotoslistState {}

class PhotoslistInitial extends PhotoslistState {}

class PhotoslistLoadingState extends PhotoslistState {}

class PhotoslistLoadedState extends PhotoslistState {
  final List<dynamic> photosDetails;
  PhotoslistLoadedState(this.photosDetails);
}

class PhotoslistErrorState extends PhotoslistState {}
