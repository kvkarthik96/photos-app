import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/Resources/RouteConst.dart';
import 'package:test_app/Screens/PhotosList/Widgets/Appbar.dart';
import 'package:test_app/Screens/PhotosList/Widgets/Shimmer.dart';
import 'package:test_app/Screens/PhotosList/bloc/photoslist_bloc.dart';
import 'package:test_app/Utils/ErrorWidget.dart';
import 'package:test_app/Utils/HelperUtil.dart';
import 'package:test_app/Utils/NoInternetUtil.dart';
import 'package:test_app/Utils/ToastUtil.dart';

class PhotosListPage extends StatefulWidget {
  final String albumId;
  final String albumName;

  const PhotosListPage({Key key, this.albumId = "", this.albumName})
      : super(key: key);
  @override
  _PhotosListPageState createState() => _PhotosListPageState();
}

class _PhotosListPageState extends State<PhotosListPage> {
  final photosListBloc = PhotoslistBloc();
  bool isNetworkConnected = false;
  var _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        isNetworkConnected = true;
        photosListBloc.add(GetPhotosetailsEvent(widget.albumId));
      } else {
        isNetworkConnected = false;
      }
    });
    photosListBloc.add(GetPhotosetailsEvent(widget.albumId));
  }

  void onNetworkChange() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
        } else {
          isNetworkConnected = true;
          photosListBloc.add(GetPhotosetailsEvent(widget.albumId));
        }
      });
    });
  }

  void dispose() {
    photosListBloc.close();
    _connectivitySubscription.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhotoslistBloc, PhotoslistState>(
        cubit: photosListBloc,
        builder: (context, state) {
          if (state is PhotoslistLoadingState) {
            return Scaffold(
                appBar: appBar(context, widget.albumName),
                body: !isNetworkConnected
                    ? NoInternetUtil(
                        retryInternetCallBack: callApiService,
                      )
                    : photosShimmerClass());
          } else if (state is PhotoslistLoadedState) {
            return Scaffold(
              appBar: appBar(context, widget.albumName),
              body: !isNetworkConnected
                  ? NoInternetUtil(
                      retryInternetCallBack: callApiService,
                    )
                  : Container(
                      child: ListView.builder(
                          itemCount: state.photosDetails.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ListTile(
                              leading: Container(
                                height: 50,
                                child: CachedNetworkImage(
                                  width: 40.0,
                                  height: 40.0,
                                  imageUrl: state.photosDetails[index]
                                          ["thumbnailUrl"]
                                      .toString(),
                                  placeholder: (context, url) => Center(
                                      child: CircularProgressIndicator()),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                              ),
                              title: Text(state.photosDetails[index]["title"]),
                              onTap: () {
                                Map<String, dynamic> data = {
                                  "imgUrl": state.photosDetails[index]
                                          ["thumbnailUrl"]
                                      .toString(),
                                  "name": state.photosDetails[index]["title"]
                                };
                                Navigator.of(context).pushNamed(
                                    RouteConst.routeViewPage,
                                    arguments: data);
                              },
                            );
                          }),
                    ),
            );
          } else {
            return Scaffold(
              appBar: appBar(context, widget.albumName),
              body: ErrorWidgetClass(
                retryInternetCallBack: callApiService,
              ),
            );
          }
        });
  }

  void callApiService() {
    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        photosListBloc.add(GetPhotosetailsEvent('1'));
      } else {
        ToastUtil().showMsg("no internet connection", Colors.black,
            Colors.white, 12.0, "short", "bottom");
      }
    });
  }
}
