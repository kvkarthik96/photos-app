import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

Widget photosShimmerClass() {
  return ListView.builder(
    itemCount: 15,
    itemBuilder: (BuildContext context, int index) {
      return Shimmer.fromColors(
        child: ListTile(
          leading: Container(
            height: 40,
            width: 40,
            color: Colors.grey[400],
          ),
          title: Container(
            height: 40,
            color: Colors.grey[400],
          ),
        ),
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[400],
      );
    },
  );
}
