import 'package:flutter/material.dart';

Widget appBar(context, albumName) {
  return AppBar(
    title: Text(albumName),
    leading: IconButton(
      icon: Icon(
        Icons.arrow_back_ios,
      ),
      color: Colors.white,
      splashColor: Colors.purple,
      onPressed: () {
        Navigator.pop(context);
      },
    ),
  );
}
